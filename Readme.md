Fitbit SDK at : https://dev.fitbit.com/
Fitbit CLI at : https://dev.fitbit.com/build/guides/command-line-interface/

Swimming icon from: https://icons8.com/icon/6084/swimming
Cardio: https://icons8.com/icon/set/cardio/material
Weights: https://icons8.com/icon/set/weights/material
Icon: https://icons8.com/icon/set/exercise/material
