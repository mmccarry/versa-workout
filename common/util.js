import { me } from "appbit";
import { listDirSync,writeFileSync,readFileSync,unlinkSync } from "fs";
import document from "document";

let save_file_name = "data.json";

let location = {
    LOADING: 0,
    PANORAMA: 1,
    WORKOUT: 2,
    QUIT_WORKOUT_DAIG: 3,
  };

let currentLocation = location.LOADING;

// show the panorama view
export function showPanoramaView() {
    document.getElementById("swipe-view").style.display = "inline";
    document.getElementById("exercise-view").style.display = "none";
    document.getElementById("quit-workout-dialog").style.display = "none";
    currentLocation = location.PANORAMA;
}

// show the exercise view
export function showExerciseView() {
    document.getElementById("swipe-view").style.display = "none";
    document.getElementById("exercise-view").style.display = "inline";
    document.getElementById("quit-workout-dialog").style.display = "none";
    currentLocation = location.WORKOUT;
}

// show the quit workout dialog
export function showQuitExerciseDialog() {
    document.getElementById("swipe-view").style.display = "none";
    document.getElementById("exercise-view").style.display = "none";
    document.getElementById("quit-workout-dialog").style.display = "inline";
    currentLocation = location.QUIT_WORKOUT_DAIG;
}

export function resetWorkoutList() {
    for (var i = 0; i < 20; i++) {
        document.getElementById("my-pool[" + i +"]").getElementById("exerciseitem").value = 0;
    }
    showPanoramaView();
}

export function backButtonPress() {
    if (currentLocation == location.PANORAMA) {
        console.log("press in panorama");
        exitApp();
    } else if (currentLocation == location.WORKOUT){
        console.log("press in workout");
        showQuitExerciseDialog();
    } else if (currentLocation == location.QUIT_WORKOUT_DAIG){
        console.log("press in quit workout diag");
        // block this, only allow exit from the buttons
        //resetWorkoutList();
    } else {
        console.log("press in elsewhere");
    }
}

export function exitApp(){
    me.exit();
}

export function getSaveFileName(){
    return save_file_name;
}

// Add zero in front of numbers < 10
export function zeroPad(i) {
   if (i < 10) {
     i = "0" + i;
   }
   return i;
}

export function disableTimeout() {
   console.log("Disabling application timeout");
   me.appTimeoutEnabled = false;
}

export function saveDataToFile(save_data){
    let dataFileName = save_file_name;
    writeFileSync(dataFileName, save_data, "json");
}

export function setUpDataFromFile (){
    let dataFileName = save_file_name;
    const listDir = listDirSync("/private/data/");
    var dirIter;

    // check to see if we have the data file
    var found_dataFileName = false;

    // look for the data file, remove other files if they exist
    while((dirIter = listDir.next()) && !dirIter.done) {
        console.log("found file " + dirIter.value);
        if(dirIter.value == dataFileName){
            found_dataFileName = true;
        } else {
            // delete the file
            unlinkSync(dirIter.value);
        }
    }

    // tmp save a json file to device
    let save_data = {
        "ex1": {"last_completed" : "No Data",},
    };

    // write the initial file if it does not exist
    if(found_dataFileName == false){
        writeFileSync(dataFileName, save_data, "json");
    }

    return readFileSync(dataFileName, "json");
}