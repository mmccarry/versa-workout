import document from "document";
import * as util from "../common/util";

// build up the panorama view from the workout data
export function buildPanoramaView(data, selectIndex, save_data){
    let actions = new Array(10);
    for(var index in data) { 
        
        // see if we have any data stored for the the last time we did this exercise
        if(data[index].id in save_data){
            console.log("found: " + data[index].id + " in save data");
        } else {
            console.log("did not find: " + data[index].id + " in save data");
            save_data[data[index].id] = {"last_completed" : "test_now"};
        }
        let last_complete = save_data[data[index].id].last_completed;
        //save the data
        util.saveDataToFile(save_data);
    
        let panorama_element = document.getElementById("workout-" + index);
        panorama_element.style.display = "inline";
        let workout_image = panorama_element.getElementById("workout-image");
        workout_image.href=data[index].icon;
        let workout_text = panorama_element.getElementById("workout-text");
        workout_text.text=data[index].display_name;
        actions[index] = panorama_element.getElementById("workout-image");
        let last_complete_text = panorama_element.getElementById("last-completed");
        last_complete_text.text = last_complete; 
    }
 
    //save the data
    util.saveDataToFile(save_data);
    
    // set the selected index
    let container = document.getElementById("container");
    container.value = selectIndex;
  
    // make the element visible
    util.showPanoramaView();
  
    return actions;
}

// Display the passed in data as a list with a done button
export function displayWorkoutList(data, panorama_index, save_data) {
   let VTList = document.getElementById("my-list");

   // add one for the done button
   let NUM_ELEMS = data.exercises.length+1;
   
   VTList.delegate = {
     getTileInfo: function(index) {
       if(index == NUM_ELEMS-1){ // catch the done button
         return {
           type: "my-button-pool",
           index: panorama_index
         }; 
       } else {
         return {
           type: "my-pool",
           header: data.exercises[index].name,
           body: data.exercises[index].metric1_type + ": " + data.exercises[index].metric1_value + " " + (data.exercises[index].metric2_value != "" ? data.exercises[index].metric2_type + ": " + data.exercises[index].metric2_value: ""),
           index: index
         };
     }
     },
     configureTile: function(tile, info) {
       if (info.type == "my-pool") {
         let mixedtext = tile.getElementById("mixedtext");
         mixedtext.text = info.header;
         let body = mixedtext.getElementById("copy");
         body.text = info.body; 
        
       }
       if(info.type == "my-button-pool") {
         let touch = tile.getElementById("close-button");
         touch.onclick = evt => {
           
          let d = new Date();
          let date_string = d.getDate() + "/" + (d.getMonth()+1) + "/" + d.getFullYear();
            
          //console.log("touched xxxxx: " + info.index );
          //console.log("id is: " + data.id);
          save_data[data.id].last_completed = date_string;
          //console.log(JSON.stringify(save_data));

          // update the last completed value on the panorama view
          let panorama_element = document.getElementById("workout-" + info.index);
          let last_complete_text = panorama_element.getElementById("last-completed");
          last_complete_text.text = date_string; 

          // save the data for the last completed date
          util.saveDataToFile(save_data, util.getSaveFileName());

          // clear all the ticked boxes on the list before exiting, remember we have only a max of 20 items per workout
          for (var i = 0; i < 20; i++) {
            document.getElementById("my-pool[" + i +"]").getElementById("exerciseitem").value = 0;
          } 

          util.showPanoramaView();
         };
       }
     }
   };
   // VTList.length must be set AFTER VTList.delegate
   VTList.length = NUM_ELEMS;
}