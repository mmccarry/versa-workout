import document from "document";
import clock from "clock";
import { HeartRateSensor } from "heart-rate";
import { preferences } from "user-settings";
import { user } from "user-profile";
import { listDirSync,writeFileSync,readFileSync,unlinkSync } from "fs";
import * as util from "../common/util";
import * as workouts from "./workouts";

// load the data from the file
let save_data  = util.setUpDataFromFile();

// Disable app timeout
util.disableTimeout();

// test load the json resources
let workout_data  = readFileSync("/mnt/assets/resources/workouts.json", "json");

// build up the panorama view from the data and set the initial value
let panorama_actions = workouts.buildPanoramaView(workout_data, 0, save_data);

// set up the callback actions for each of the panorama actions
panorama_actions.forEach((element, index) => {
  element.onclick = (evt) => {
    // hide the panorama view and show the 
    util.showExerciseView();
    workouts.displayWorkoutList(workout_data[index], index, save_data ); 
  }
});

// capture the physical buttons and override the back button if we not at the top level
// panorama view.
document.onkeypress = function(e) {
  e.preventDefault();
  if (e.key==="back") {
      util.backButtonPress();
  }
}

// catch the buttons from the workout close dialog
let dialogBtnQuit = document.getElementById("quit-workout-dialog").getElementById("dialog-btn-quit");
let dialogBtnResume = document.getElementById("quit-workout-dialog").getElementById("dialog-btn-resume");
dialogBtnQuit.onclick = evt => {
    util.resetWorkoutList();
}
dialogBtnResume.onclick = evt => {
    util.showExerciseView();
}

// Initialize the UI with some values
let hrLabel = document.getElementById("hrm");
let hrImage = document.getElementById("heart-image");
hrLabel.text = "--";

// Set up to monitor the heart rate
let hrm = new HeartRateSensor();
hrm.onreading = function() {
  hrLabel.text = hrm.heartRate;
  // work out the heart rate zone
  var heartRateZone = user.heartRateZone(hrm.heartRate);
  var heartcolor = "white";
  if(heartRateZone == "out-of-range"){
    heartcolor = "white";
  } else if (heartRateZone == "fat-burn"){
    heartcolor = "green";
  } else if (heartRateZone == "cardio"){
    heartcolor = "orange";
  } else if (heartRateZone == "peak") {
    heartcolor = "red";
  }
  // if we are over HR Max alert by purple, if we have the users age, work it out
  // by subtracting age from 220
  let maxRate = 190;

  if(user.age != "undefined"){
     maxRate = 220 - user.age;
  }
   
  if(hrm.heartRate >= maxRate){
    heartcolor = "purple"; 
  }

  hrImage.style.fill = heartcolor;
}
hrm.start();

// Update the clock every minute
clock.granularity = "minutes";

// initalize the time elements
let timeLabel = document.getElementById("time");
// Have some way to record the date from the clock tick
let today = "";

clock.ontick = (evt) => {
  // update the today field
  today = evt.date;
  let hours = today.getHours();
  if (preferences.clockDisplay === "12h") {
    // 12h format
    hours = hours % 12 || 12;
  } else {
    // 24h format
    hours = util.zeroPad(hours);
  }
  let mins = util.zeroPad(today.getMinutes());
  timeLabel.text = hours + ":" + mins;
}